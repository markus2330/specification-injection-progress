sudo kdb mount test.dump /test path dump
sudo kdb setmeta /test/path check/path ""
sudo kdb setmeta /test/path check/path/user "wespe"
sudo kdb setmeta /test/path check/path/mode "rw"

# Generate a file with restrictive permissions
touch /tmp/testfile.txt
sudo chmod 700 /tmp/testfile.txt
sudo chown root:root /tmp/testfile.txt

# The following command has to be done as root
sudo kdb set /test/path "/tmp/testfile.txt"
> Using name system/test/path
> Sorry, the error (#207) occurred ;(
> Description: Detected incorrect permissions for file/directory
> Reason: User tomcat does not have [read,write] permission on /tmp/testfile.txt
> Ingroup: plugin
> Module: path
> At: ....../path.c:224
> Mountpoint: system/test
> Configfile: /etc/kdb/test.dump.5838:1547304979.131617.tmp
> Create a new key system/test/path with string "/tmp/testfile.txt"

# Fix permissions
sudo chmod 777 /tmp/testfile.txt

sudo kdb set /test/path "/tmp/testfile.txt"
> Using name system/test/path
> Set string to "/tmp/testfile.txt"

#cleanup
sudo kdb rm -r /test
sudo kdb umount /test
sudo rm /tmp/testfile.txt