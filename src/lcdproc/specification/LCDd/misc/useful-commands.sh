sudo kdb mount LCDd.conf spec/lcd dump
sudo kdb import spec/lcd ni < LCDd-spec.ini
kdb spec-mount /lcd

kdb rm -r spec/lcd
kdb rm -r /lcd
kdb umount spec/lcd
kdb umount /lcd

alias reload="kdb rm -r spec/lcd; kdb import spec/lcd ni < LCDd-spec.ini && kdb spec-mount /lcd"