[/server]
define/type=
define/type/driver=
define/type/driver/check/enum=#43
# they should be populated by "make"
define/type/driver/check/enum/#0=bayrad
define/type/driver/check/enum/#1=CFontz
define/type/driver/check/enum/#2=CFontzPacket
define/type/driver/check/enum/#3=curses
define/type/driver/check/enum/#4=CwLnx
define/type/driver/check/enum/#5=ea65
define/type/driver/check/enum/#6=EyeboxOne
define/type/driver/check/enum/#7=futaba
define/type/driver/check/enum/#8=g15
define/type/driver/check/enum/#9=glcd
define/type/driver/check/enum/#10=glcdlib
define/type/driver/check/enum/#11=glk
define/type/driver/check/enum/#12=hd44780
define/type/driver/check/enum/#13=icp_a106
define/type/driver/check/enum/#14=imon
define/type/driver/check/enum/#15=imonlcd
define/type/driver/check/enum/#16=IOWarrior
define/type/driver/check/enum/#17=mdm166a
define/type/driver/check/enum/#18=ms6931
define/type/driver/check/enum/#19=mtcs16209x
define/type/driver/check/enum/#20=MtxOrb
define/type/driver/check/enum/#21=mx5000
define/type/driver/check/enum/#22=NoritakeVFD
define/type/driver/check/enum/#23=Olimex_MOD_LCD1x9
define/type/driver/check/enum/#24=picolcd
define/type/driver/check/enum/#25=pyramid 
define/type/driver/check/enum/#26=rawserial
define/type/driver/check/enum/#27=sdeclcd
define/type/driver/check/enum/#28=sed1330
define/type/driver/check/enum/#29=sed1520
define/type/driver/check/enum/#30=serialPOS
define/type/driver/check/enum/#31=serialVFD
define/type/driver/check/enum/#32=shuttleVFS
define/type/driver/check/enum/#33=sli
define/type/driver/check/enum/#34=stv5730
define/type/driver/check/enum/#35=svga
define/type/driver/check/enum/#36=t6963
define/type/driver/check/enum/#37=text
define/type/driver/check/enum/#38=tyan
define/type/driver/check/enum/#39=ula200
define/type/driver/check/enum/#40=vlsys_m428
define/type/driver/check/enum/#41=xosd
define/type/driver/check/enum/#42=yard2LCD
define/type/serverkey=
define/type/serverkey/check/enum=#5
define/type/serverkey/check/enum/#0=Escape
define/type/serverkey/check/enum/#1=Enter
define/type/serverkey/check/enum/#2=Up
define/type/serverkey/check/enum/#3=Down
define/type/serverkey/check/enum/#4=Left
define/type/serverkey/check/enum/#5=Right


# Where can we find the driver modules ?
# IMPORTANT: Make sure to change this setting to reflect your
#            specific setup! Otherwise LCDd won't be able to find
#            the driver modules and will thus not be able to
#            function properly.
# NOTE: Always place a slash as last character !
[/server/DriverPath]
type=string
default="server/drivers/"

# Tells the server to load the given drivers. Multiple lines can be given.
# The name of the driver is case sensitive and determines the section
# where to look for further configuration options of the specific driver
# as well as the name of the dynamic driver module to load at runtime.
# The latter one can be changed by giving a File= directive in the
# driver specific section.
[/server/Driver]
opt=d
type=driver
default=curses

# no, no no! needs a better solution !
[/server/Driver/#]
opt=d
type=driver
default=curses

[/server/Bind]
type=string
check/ipaddr=ipv4
default="127.0.0.1"
opt=a

[/server/Port]
type=unsigned_short
check/range=1-65535
default=13666
opt=p

[/server/ReportLevel]
type=unsigned_short
check/range=0-5
default=3
opt=r

[/server/ReportToSyslog]
opt=s
type=boolean
default=no

[/server/User]
type=string
default="nobody"
opt=u

[/server/Foreground]
type=boolean
default=no
opt=f

[/server/FrameInterval]
type=unsigned_long
default=125000
check/type=unsigned_long

[/server/WaitTime]
type=unsigned_short
default=4
check/type=unsigned_short
opt=w


# If set to no, LCDd will start with screen rotation disabled. This has the
# same effect as if the ToggleRotateKey had been pressed. Rotation will start
# if the ToggleRotateKey is pressed. Note that this setting does not turn off
# priority sorting of screens. [default: on; legal: on, off]
[/server/AutoRotate]
type=enum
default=on
check/enum=#2
check/enum/#0=off
check/enum/#1=on
check/enum/#2=blank


# If yes, the the serverscreen will be rotated as a usual info screen. If no,
# it will be a background screen, only visible when no other screens are
# active. The special value 'blank' is similar to no, but only a blank screen
# is displayed. [default: on; legal: on, off, blank]
[/server/ServerScreen]
opt=i
type=enum
default=on
check/enum=#2
check/enum/#0=off
check/enum/#1=on
check/enum/#2=blank


# Set master backlight setting. If set to 'open' a client may control the
# backlight for its own screens (only). [default: open; legal: off, open, on]
[/server/Backlight]
type=enum
default=open
check/enum=#2
check/enum/#0=off
check/enum/#1=on
check/enum/#2=open


# Set master heartbeat setting. If set to 'open' a client may control the
# heartbeat for its own screens (only). [default: open; legal: off, open, on]
[/server/Heartbeat]
type=enum
default=open
check/enum=#2
check/enum/#0=off
check/enum/#1=on
check/enum/#2=open


# set title scrolling speed [default: 10; legal: 0-10]
[/server/TitleSpeed]
type=unsigned short
default=10
check/range=0-10


[/server/ToggleRotateKey]
type=serverkey
default=Enter

[/server/PrevScreenKey]
type=serverkey
default=Left

[/server/NextScreenKey]
type=serverkey
default=Right

[/server/ScrollUpKey]
type=serverkey
default=Up

[/server/ScrollDownKey]
type=serverkey
default=Down

[/server/configfile]
type=string
default="/sw/lcdproc/lcdproc/#0/current"
opt=c

[/menu]
define/type=
define/type/menukey=
define/type/menukey/check/enum=#5
define/type/menukey/check/enum/#0=Escape
define/type/menukey/check/enum/#1=Enter
define/type/menukey/check/enum/#2=Up
define/type/menukey/check/enum/#3=Down
define/type/menukey/check/enum/#4=Left
define/type/menukey/check/enum/#5=Right


[/menu/MenuKey]
type=menukey
default=Escape

[/menu/EnterKey]
type=menukey
default=Enter

[/menu/UpKey]
type=menukey
default=Up

[/menu/DownKey]
type=menukey
default=Down

[/menu/LeftKey]
type=menukey
default=Left

[/menu/RightKey]
type=menukey
default=Right

[/curses]
define/type=
define/type/cursescolor=
define/type/cursescolor/check/enum=#7
# constants from curses.h
define/type/cursescolor/check/enum/#0=black
define/type/cursescolor/check/enum/#1=red
define/type/cursescolor/check/enum/#2=green
define/type/cursescolor/check/enum/#3=yellow
define/type/cursescolor/check/enum/#4=blue
define/type/cursescolor/check/enum/#5=magenta
define/type/cursescolor/check/enum/#6=cyan
define/type/cursescolor/check/enum/#7=white

[/curses/Foreground]
type=cursescolor
default=blue
gen/type=string 

[/curses/Background]
type=cursescolor
default=cyan

[/curses/Backlight]
type=cursescolor
default=red

[/curses/Size]
type=string
default="20x4"

[/curses/TopLeftX]
type=unsigned_short
default=7

[/curses/TopLeftY]
type=unsigned_short
default=7

[/curses/UseACS]
type=boolean
default=no

[/curses/DrawBorder]
type=boolean
default=yes

