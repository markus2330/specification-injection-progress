## lcdproc
- [ ] ReportLevel
 - Allowed Range?

- [ ] Delay
 - Any positive number I guess?
 - 0 included?
 - Default value?

 ## CPU
 - [ ] OnTime/Offtime
    - Description is missing
    - Just positive number? 
    - 0 included?

- [ ] ShowInvisible
    - Any Description?

 ## @Markus
 - [ ] PidFile
    - Just gets created when starting I guess. So only permission to create in folder?

- [ ] Load: LoadLoad & HighLoad
    - float given, range plugin not capable of
    - Conditional Plugin most likely cant guarantee low > high I guess?

- [ ] DateFormat, TimeFormat
    - User specifies the format himself: `time` plugin not senseful in this case I assume
    - User should specify in [strftime(3)](http://man7.org/linux/man-pages/man3/strftime.3.html) format.
    - Validation possible at all? Maybe regex?