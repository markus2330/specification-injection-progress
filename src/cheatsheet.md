# Cheat Sheet

## Mount a specification

kdb set '/sw/lcdproc/lcdd/#0/current/server/driverpath' /usr/local/lib/lcdproc/

### LDCd

```sh
kdb umount 'spec/sw/lcdproc/lcdd/#0/current'

kdb mount '/home/wespe/extern/repository/lcdproc/server/specification/LCDd-spec.ini' 'spec/sw/lcdproc/lcdd/#0/current' ni
kdb spec-mount '/sw/lcdproc/lcdd/#0/current'

kdb set '/sw/lcdproc/lcdd/#0/current/driver/curses/backlight' green
```

### LCDproc
```sh
kdb umount 'spec/sw/lcdproc/lcdproc/#0/current'

kdb mount '/home/wespe/extern/repository/lcdproc/clients/lcdproc/specification/lcdproc-spec.ini' 'spec/sw/lcdproc/lcdproc/#0/current' ni
kdb spec-mount '/sw/lcdproc/lcdproc/#0/current'

kdb set '/sw/lcdproc/lcdproc/#0/current/screenmode/about/active' False
```

### LCDexec
```sh
kdb umount 'spec/sw/lcdproc/lcdexec/#0/current'

kdb mount '/home/wespe/extern/repository/lcdproc/clients/lcdexec/specification/lcdexec-spec.ini' 'spec/sw/lcdproc/lcdexec/#0/current' ni
kdb spec-mount '/sw/lcdproc/lcdexec/#0/current'

kdb set '/sw/lcdproc/lcdexec/#0/current/general/menu/menu/#0/displayname' "Menu Entry"
kdb set '/sw/lcdproc/lcdexec/#0/current/general/menu/menu/#0/exec' 'echo Working!'
kdb set '/sw/lcdproc/lcdexec/#0/current/general/menu/menu/#0/feedback' "yes"
kdb set '/sw/lcdproc/lcdexec/#0/current/general/menu/main' "../../menu/menu/#0"
```

### LCDvc
```sh
kdb umount 'spec/sw/lcdproc/lcdvc/#0/current'

kdb mount '/home/wespe/extern/repository/lcdproc/clients/lcdvc/specification/lcdvc-spec.ini' 'spec/sw/lcdproc/lcdvc/#0/current' ni
kdb spec-mount '/sw/lcdproc/lcdvc/#0/current'

kdb set '/sw/lcdproc/lcdvc/#0/current/port' 13667
```


### Cassandra
```
kdb mount /home/wespe/extern/repository/lcdproc-specification/src/cassandra/specification.ini 'spec/cassandra' ni
kdb spec-mount '/cassandra'

kdb umount 'spec/cassandra'
kdb umount '/cassandra'
```


```
kdb mount test.spec spec/tests dump
kdb mount test.conf /tests dump enum

# sudo kdb setmeta spec/tests/abc type enum
# sudo kdb setmeta spec/tests/abc check/type enum
kdb setmeta spec/tests/abc 'check/enum' '#7'
kdb setmeta spec/tests/abc 'check/enum/#0' red
kdb setmeta spec/tests/abc 'check/enum/#1' black
kdb setmeta spec/tests/abc 'check/enum/#2' green
kdb setmeta spec/tests/abc 'check/enum/#3' yellow
kdb setmeta spec/tests/abc 'check/enum/#4' blue
kdb setmeta spec/tests/abc 'check/enum/#5' magenta
kdb setmeta spec/tests/abc 'check/enum/#6' cyan
kdb setmeta spec/tests/abc 'check/enum/#7' white

kdb set /tests/abc redd

kdb umount spec/tests
kdb umount /tests
```