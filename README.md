# lcdproc-specification

This project will reflect my progress on writing configuration specifications for [LCDproc](http://lcdproc.omnipotent.net/). LCDproc will use [elektra](https://www.libelektra.org/home) as their new configuration tool.

## Blockers

- [x] ["too many plugins" error](https://github.com/ElektraInitiative/libelektra/issues/2133)
    - By using JNA and manually setting plugins I do not have this error anymore

## TODO's

- [x] Write specification for LCDd (server)
- [x] Write specification for lcdvc (client)
- [x] Write specification for lcdexec (client)
- [x] Write specification for lcdproc (client)
- [ ] Write a tutorial for lcdproc users on how they can change their settings
- [ ] Write the injection tool
- [x] Running the typchecker
    - **Should ignore it as it is too troublesome**
- [ ] How to deal with default values that depend on other settings
    - eg. CFontzPacket/Speed: Default value depends on model
- [x] Maybe specification possible to avoid that some keys may not be set twice
    - eg. `/Computer/PowerOff EnterKey` and `/Computer/OpenFolder EnterKey`
    - **Not needed and keys can be arbitraty, even those who do not yet exist**
